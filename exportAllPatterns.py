#
# Export a Virtual System Pattern from IBM Workload Deployer.
#
# This script is called as follows:
#
# deployer <deployer_options> -f /path/to/exportPattern.py

import getopt
import sys
import shutil
import os
from deployer.messages import message
from deployer import ziputils
from deployer import utils

i = 0
for p in deployer.virtualsystempatterns.list({'patterntype':'vsys'}):
    filename = '/root/exportPatterns/' + p.app_name.replace(" ", "_") + '.zip'
    print filename
    try:
        os.remove(filename)
    except OSError:
        pass
    deployer.virtualsystempatterns[i].download(filename)
    i = i + 1

sys.exit(1)