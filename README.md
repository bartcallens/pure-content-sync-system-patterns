# README #

Pure Application Content Sync for Virtual System Patterns

### What is this repository for? ###

With the provided scripts you can download/upload patterns with use of the pure.cli

### How do I get set up? ###

Download the pure.cli from the homepage of the pure application platform and put it on a server
Place .py scripts on the server

### How to download all virtual system patterns?

cd </path/to/pure-cli/bin>
./pure -h h0000590 -u <username> -p <password> -a -f /location/of/exportAllPatterns.py
--> all pattern can be found on /root/exportPatterns


### How to import a virtual system pattern
cd </path/to/pure-cli/bin>
./pure -h h0000591 -u <username> -p <password> -a -f /location/of/importPattern.py -s /location/of/exported_pattern.zip
--> the virtual system pattern will be created on the rack