"""
#*===================================================================
#*
#* Licensed Materials - Property of IBM
#* IBM Workload Deployer (7199-72X)
#* Copyright IBM Corporation 2009, 2018. All Rights Reserved.
#* US Government Users Restricted Rights - Use, duplication or disclosure
#* restricted by GSA ADP Schedule Contract with IBM Corp.
#*
#*===================================================================
"""
# Import Virtual System Patterns to IBM Workload Deployer
#
# This script is called as follows:
#
# deployer <deployer_options> -f samples/importPatterns.py
#     -s|--source <dirname>
#
# Where:
#
# -s|--source <dirname>
#     Specifies a directory or .tgz/.tar.gz file for pattern import
#


import getopt
import sys
import os
import shutil
import tempfile
from deployer import ziputils
from deployer.messages import message
from deployer import utils

# print help and exit
def help():
    if utils.isIPAS():
        command = 'pure'
        option = 'RM09989'
    else:
        command = 'deployer'
        option = 'RM09988'
    print utils.utos(message('IWD10050') % ({'1': command, '2': message(option)}))
    sys.exit(1)


def _utos(u):
    if isinstance(u, unicode):
        s = ''
        for b in java.lang.String(u).getBytes():
            s += chr(b & 0xff)
    else:
        s = str(u)
    return s


options =[]
# parse command line arguments
try:
    (options, args) = getopt.getopt(sys.argv[1:], 's:', ['source='])
except getopt.GetoptError:
    help()


source = None

for option in options:
    if option[0] == '-s' or option[0] == '--source':
        source = option[1]

if (source is None):
    print >>sys.stderr, utils.utos(message('IWD10052'))
    help()
    sys.exit(1)

if (not os.path.exists(source)):
    print >>sys.stderr, utils.utos(message('IWD10053') % source)
    sys.exit(1)

if source.endswith('.zip'):
    zipped = True
else:
    print >>sys.stderr, utils.utos(message('IWD10054') % source)
    sys.exit(1)

deployer.virtualsystempatterns.create(source, replace = True)
